<?php

define("DISPLAY_ERROR", 1);
error_reporting(E_ALL);
ini_set("display_errors", DISPLAY_ERROR);


//emplacement du projet album_photo sur le disque dur
define("WAY", "C:/wamp64/www/album_photo/files/");

//URL d'accès au site internet
define("URL", "http://localhost/album/");//accès au site internet

//nom de la base de données
define("BASE_NAME", "album_photo");

//hôte de la base de données
define("SQL_HOST", "localhost");

//Nom d'utilisateur de l'utilisateur de la base de données
define("SQL_USER", "root");

//Mot de passe de l'utilisateur de la base de données
define("SQL_PASSWORD", "123456");

//Adresse Email Utilisée pour l'envoi du mail de récupération de mot de passe
define("MAIL_RECOVERY", "passwordrecovery@album_photo.ch");

?>