<?php
Class Photo EXTENDS Projet{

    // PARAMETERS
    private $id;
    private $titre;
    private $desc;
    private $hash;
    private $date;
    private $filename;
    private $id_per;


    /**
     * Constructeur de la fonction
     * @param null $id
     */
    public function __construct($id = null) {

        $this->table_name = "t_photos";
        $this->suffix = "_pho";

        parent::__construct();

        if($id){
            $this->set_id($id);
            $this->init();
        }

    }

    /**
     * Initialisation de l'objet
     * @return bool
     */
    public function init() {

        $query = "SELECT * FROM t_photos WHERE id_pho=:id_pho";
        try {

            $stmt = $this->pdo->prepare($query);
            $args['id_pho'] = $this->get_id();
            $stmt->execute($args);
            $tab = $stmt->fetch();

            $this->set_titre($tab['titre_pho']);
            $this->set_desc($tab['desc_pho']);
            $this->set_date($tab['date_pho']);
            $this->set_filename($tab['filename_pho']);
            $this->set_idPer($tab['id_per']);
            $this->set_hash($tab['hash_pho']);
            return true;
        } catch (Exception $e) {

            return false;
        }
        return true;
    }

    /**
     * Fonction de base toString
     * @return string
     */
    public function __toString() {

        $str = "\n<pre>\n";
        foreach($this as $key => $val){
            if($key != "pdo"){
                $str .= "\t" . $key;
                $lengh_key = strlen($key);
                for($i = $lengh_key; $i < 20;$i++) {
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;".$val."\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }

    /**
     * Ajout d'une photo dans la base
     * @param $tab
     * @return bool|string
     */
    public function add($tab,$debug=0){
        $tab['id_pho'] = parent::add($tab,$debug);
        $this->set_id($tab['id_pho']);
        $this->set_filename($tab['filename_pho']);
        $this->set_date($tab['date_pho']);
        $this->set_idPer($tab['id_per']);
        $tab['hash_pho'] = $this->gen_hash();
        if($debug) {
            echo "<pre>";
            print_r($tab);
            echo "</pre>";
        }
        if($this->update($tab)){
            return $this->get_id();
        }else {
            parent::remove($this->get_id());
            return false;
        }
    }

    public function add_alb($tab){
        $query = "SELECT id_alb from t_albums "
            ."WHERE nom_alb =:nom_alb AND id_per =:id_per";
        $args['nom_alb'] = $tab['nom_alb'];
        $args['id_per'] = $tab['id_per'];
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
        $tab += $stmt->fetch();

        $query = "SELECT MAX(order_pho) order_pho FROM t_alb_pho "
            ."WHERE id_alb = :id_alb";
        $args = array();
        $args['id_alb'] = $tab['id_alb'];
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
        $tab += $stmt->fetch();
        if($tab['order_pho'] == null){
            $tab['order_pho'] = -1;
        }

        if($tab['id_alb']) {
            $query = "INSERT INTO t_alb_pho SET id_pho =:id_pho, id_alb =:id_alb, order_pho =:order_pho";
            $args = array();
            $args['id_pho'] = $this->get_id();
            $args['id_alb'] = $tab['id_alb'];
            $args['order_pho'] = $tab['order_pho'] + 1;
            $stmt = $this->pdo->prepare($query);
            $tab['execute'] = $stmt->execute($args);
            return $tab;
        }
        return false;
    }

    public function add_tag($tab){
        $query = "SELECT id_tag from t_tags "
            ."WHERE nom_tag =:nom_tag";
        $args['nom_tag'] = $tab['nom_tag'];
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
        $tab += $stmt->fetch();

        if($tab['id_tag']) {
            $query = "INSERT INTO t_pho_tag SET id_pho =:id_pho, id_tag =:id_tag";
            $args = array();
            $args['id_pho'] = $this->get_id();
            $args['id_tag'] = $tab['id_tag'];
            $stmt = $this->pdo->prepare($query);
            $tab['execute'] = $stmt->execute($args);
            return $tab;
        }
        return false;
    }

    public function remove($id,$debug = 0){
        $pho = new Photo($id);
        $per = new Personne($pho->get_idPer());
        $folders = explode("-", $pho->get_date());
        $way = WAY."../photos/".$per->get_hash()."/".$folders[0]."/".$folders[1]."/".$folders[2]."/".$pho->get_hash().".".explode(".", $pho->get_filename())[1];

        unlink($way);
        $this->order_after_delete($id);
        return parent::remove($id, $debug);
    }

    public function remove_alb($id_alb_pho){
        $query = "SELECT id_pho,id_alb,order_pho FROM t_alb_pho WHERE id_alb_pho=:id_alb_pho";
        $args['id_alb_pho'] = $id_alb_pho;
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
        $id = $stmt->fetch();
        $this->order_after_delete($id['id_pho'],$id['id_alb'],$id['order_pho']);

        $query = "DELETE FROM t_alb_pho WHERE id_alb_pho=:id_alb_pho";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
    }

    public function remove_tag($id_pho_tag){
        $query = "SELECT id_pho,id_tag FROM t_pho_tag WHERE id_pho_tag=:id_pho_tag";
        $args['id_pho_tag'] = $id_pho_tag;
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
        $id = $stmt->fetch();
        $this->order_after_delete($id['id_pho'],$id['id_tag']);

        $query = "DELETE FROM t_pho_tag WHERE id_pho_tag=:id_pho_tag";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
    }

    public function order_after_delete($id,$id_alb = 0,$order_pho = 0){
        $query = "SELECT id_alb,order_pho FROM t_alb_pho WHERE id_pho = :id_pho";
        $args['id_pho'] = $id;
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
        $tab = $stmt->fetchAll();
        if($id_alb) {
            $tab = array();
            $tab[0]['id_alb'] = $id_alb;
            $tab[0]['order_pho'] = $order_pho;
        }
        foreach($tab as $value){
            $query = "UPDATE t_alb_pho SET order_pho = (order_pho - 1) "
                ."WHERE id_alb = :id_alb AND order_pho > :order_pho";
            $args = array();
            if($id_alb){
                $args['id_alb'] = $id_alb;
            }else{
                $args['id_alb'] = $value['id_alb'];
            }
            $args['order_pho'] = $value['order_pho'];
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
        }
    }

    /**
     * Récupération de toutes les photos
     * @parm $order
     * @return array | bool
     */
    public function get_photos($order = "nom_alb") {

        $args[':order'] = $order;
        $query = "SELECT * FROM t_photos ORDER BY :order";

        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return($tab);

        } catch(Exception $e) {
            return false;
        }
    }

    public function get_all_alb(){
        $args['id_pho'] = $this->get_id();
        $query = "SELECT * FROM t_alb_pho WHERE id_pho = :id_pho";

        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return($tab);

        } catch(Exception $e) {
            return false;
        }
    }

    public function get_all_alb_not_used($id_per){
        $query = "SELECT DISTINCT(ALB.id_alb), ALB.nom_alb FROM t_albums ALB "
            ."LEFT JOIN t_alb_pho ALP on ALB.id_alb = ALP.id_alb "
            ."WHERE ALB.id_per = :id_per AND alb.id_alb NOT IN(SELECT id_alb FROM t_alb_pho WHERE id_pho = :id_pho)";

        try {
            $args['id_per'] = $id_per;
            $args['id_pho'] = $this->get_id();
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return($tab);

        } catch(Exception $e) {

            return false;
        }
    }

    /**
     * Récupération de tous les tags non associé à la photo sélectionnée
     * @author cp-17nod
     * @return array
     */
    public function get_all_tag_not_used(){
        $query = "SELECT DISTINCT(TAG.id_tag), TAG.nom_tag FROM t_tags TAG "
            ."LEFT JOIN t_pho_tag TAP on TAG.id_tag = TAP.id_tag "
            ."WHERE TAG.id_tag NOT IN(SELECT id_tag FROM t_pho_tag WHERE id_pho = :id_pho)";

        try {
            $args['id_pho'] = $this->get_id();
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return($tab);

        } catch(Exception $e) {

            return false;
        }
    }

    /**
     * Récupération de toutes les données utiles dans un tableau
     * @param $id
     * @author cp-17jbr
     * @return array
     */
    public function get_details($id=0){
        if(!$id){
            $id = $this->get_id();
        }
        $args['id_pho'] = $id;
        $query = "SELECT * FROM t_photos "
            ."WHERE id_pho=:id_pho";

        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
        $tab = $stmt->fetch();

        $query = "SELECT * FROM t_photos PHO "
            ."JOIN t_alb_pho ALP ON PHO.id_pho = ALP.id_pho "
            ."JOIN t_albums ALB ON ALP.id_alb = ALB.id_ALB "
            ."WHERE PHO.id_pho=:id_pho";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
        $tab['alb'] = $stmt->fetchAll();

        $query = "SELECT * FROM t_photos PHO "
            ."JOIN t_pho_tag TAP ON PHO.id_pho = TAP.id_pho "
            ."JOIN t_tags tag ON TAP.id_tag = tag.id_tag "
            ."WHERE PHO.id_pho=:id_pho";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
        $tab['tag'] = $stmt->fetchAll();

        $tab['way_pho'] = $this->get_way();
        $tab['model_pho'] = "Inconnu";
        $tab['software_pho'] = "Inconnu";
        if($this->get_extention() == "jpg"){
            if(isset(exif_read_data($this->get_way())['Model'])) {
                $tab['model_pho'] = exif_read_data($this->get_way())['Model'];
            }
            if(isset(exif_read_data($this->get_way())['Software'])) {
                $tab['software_pho'] = exif_read_data($this->get_way())['Software'];
            }
            $tab['exif'] = exif_read_data($this->get_way());
        }
        return $tab;
    }

    /**
     * Récupération de l'extention de l'image
     * @author cp-17nod
     * @return string
     */
        public function get_extention(){
            $extention = strtolower(substr($this->get_filename(),strrpos($this->get_filename(),".")+1));
            return $extention;
        }

        /**
         * Récupération du nom de l'image
         * @author cp-17nod
         * @return string
         */
        public function get_nom_img(){
            $nom_img = explode(".", $this->get_filename())[0];
            return $nom_img;
        }

        /**
         * Récupération du chemin de l'image
         * @param $id
         * @author cp-17jbr
         * @return string
         */
        public function get_way($id=0){
            if(!$id){
                $id = $this->get_id();
            }
            $pho = new Photo($id);
            $per = new Personne($pho->get_idPer());
            $folders = explode("-", $pho->get_date());
            $way = WAY."../photos/".$per->get_hash()."/".$folders[0]."/".$folders[1]."/".$folders[2]."/".$pho->get_hash().".".explode(".", $pho->get_filename())[1];
            return $way;
    }

    /**
     * Hashage du nom de la photo par son filename
     * @author cp-17jbr
     * @return mixed
     */
    public function gen_hash(){
        $this->set_hash(md5($this->get_idPer().$this->get_filename()));
        return $this->get_hash();
    }

    /*** Set et get ***/

    public function set_id($id) {
        $this->id = $id;
    }
    public function get_id() {
        return $this->id;
    }

    public function set_hash($hash)
    {
        $this->hash = $hash;
    }
    public function get_hash()
    {
        return $this->hash;
    }

    public function get_titre()
    {
        return $this->titre;
    }
    public function set_titre($titre)
    {
        $this->titre = $titre;
    }

    public function get_desc()
    {
        return $this->desc;
    }
    public function set_desc($desc)
    {
        $this->desc = $desc;
    }

    public function get_date()
    {
        return $this->date;
    }
    public function set_date($date)
    {
        $this->date = $date;
    }

    public function get_filename()
    {
        return $this->filename;
    }
    public function set_filename($filename)
    {
        $this->filename = $filename;
    }

    public function get_idPer()
    {
        return $this->id_per;
    }
    public function set_idPer($id_per)
    {
        $this->id_per = $id_per;
    }

}
