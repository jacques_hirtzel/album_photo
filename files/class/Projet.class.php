<?php
Class Projet {

    protected $pdo;
    protected $table_name;
    protected $suffix;

    /**
     * Constructeur de la fonction
     * @param string $data_base
     */
    public function __construct($data_base = "") {

        switch ($data_base){
            case "":
                $this->pdo = new PDO('mysql:dbname=' . BASE_NAME . ';host=' . SQL_HOST, SQL_USER, SQL_PASSWORD,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC));
            break;

            case "CMSMS":
                $this->pdo = new PDO('mysql:dbname=' . BASE_NAME_CMSMS . ';host=' . SQL_HOST_CMSMS , SQL_USER_CMSMS , SQL_PASSWORD_CMSMS ,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC));
            break;
        }
    }


    public function list_of_fields($debug = 0){
        $query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '".$this->table_name."' AND TABLE_SCHEMA='".BASE_NAME."'";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        $tab_tmp = $stmt->fetchAll();
        $tab_fields = array();
        foreach ($tab_tmp AS $column){
            $tab_fields[] = $column['COLUMN_NAME'];
        }
        if($debug) {
            echo "<pre>";
            echo "<br>";
            print_r($tab_fields);
            echo "<br>";
        }
        return $tab_fields;
    }

    /**
     * Modifie un ou plusieurs champs de la base de données
     * @author CP-JHI & CP-17JBR
     * @date 04.11.2019
     * @param $tab
     * @param int $debug
     * @return bool
     */
    public function update($tab,$debug=0){
        $tab_fields = $this->list_of_fields();
        $fields = "";
        foreach ($tab AS $field => $value){
            if(in_array($field,$tab_fields)) {
                if ($field != "id" . $this->suffix) {
                    $fields .= " " . $field . "=:" . $field . ",";
                }
            }else{
                unset($tab[$field]);
            }
        }
        $fields = substr($fields,0,-1);
        $query =  "UPDATE ".$this->table_name. " SET".$fields. " WHERE id".$this->suffix." = :id".$this->suffix;
        $stmt = $this->pdo->prepare($query);

        if($debug){
            echo "<pre>";
            print_r($tab_fields);
            echo "<br>";
            echo $query;
            echo "<br>";
            print_r($tab);
            echo "</pre>";
        }
        return $stmt->execute($tab);
    }


    /**
     * Ajout un champs dans la base de données
     * @author CP-17JBR
     * @date 04.11.2019
     * @param $tab
     * @param int $debug
     * @return bool
     */
    public function add($tab,$debug=0){
        $tab_fields = $this->list_of_fields($debug);

        $fields = "";
        $values = "";
        foreach ($tab AS $field => $value){
            if(in_array($field,$tab_fields)) {
                if ($field != "id" . $this->suffix) {
                    $fields .= "" . $field . ", ";
                    $values .= ":" . $field . ", ";

                }else{
                    unset($tab[$field]);
                }
            }else{
                unset($tab[$field]);
            }
        }
        $fields = substr($fields,0,-2);
        $values = substr($values,0,-2);
        $query =  "INSERT INTO ".$this->table_name. " (".$fields. ") VALUES (".$values.")";
        $stmt = $this->pdo->prepare($query);

        if($debug){
            echo "<pre>";
            echo $query;
            echo "<br>";
            print_r($tab);
            echo "<br>";
        }
        $stmt->execute($tab);

        return $this->pdo->lastInsertId();
    }

    /**
     * Efface un champ dans la base de données
     * @author cp-17jbr
     * @date 11.11.2019
     * @param $id
     * @param int $debug
     * @return bool
     */
    public function remove($id,$debug=0){
        $args['id'] = $id;
        $query = "DELETE FROM ".$this->table_name." WHERE id".$this->suffix." = :id";

        $stmt = $this->pdo->prepare($query);
        return $stmt->execute($args);
    }

    public function rrmdir($path) {
        // Open the source directory to read in files
        $i = new DirectoryIterator($path);
        foreach($i as $f) {
            if($f->isFile()) {
                unlink($f->getRealPath());
            } else if(!$f->isDot() && $f->isDir()) {
                $this->rrmdir($f->getRealPath());
            }
        }
        rmdir($path);
    }
}