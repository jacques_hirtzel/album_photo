<?php

class Album EXTENDS Projet
{
    private $id;
    private $nom;
    private $desc;
    private $acces;

    public function __construct($id = null) {

        $this->table_name = "t_albums";
        $this->suffix = "_alb";

        parent::__construct();

        if($id){
            $this->set_id($id);
            $this->init();
        }
    }

    public function init() {

        $query = "SELECT * FROM t_albums WHERE id_alb=:id_alb";
        try {

            $stmt = $this->pdo->prepare($query);
            $args['id_alb'] = $this->get_id();
            $stmt->execute($args);
            $tab = $stmt->fetch();

            $this->set_nom($tab['nom_alb']);
            $this->set_desc($tab['desc_alb']);
            $this->set_acces($tab['acces_alb']);
            return true;
        } catch (Exception $e) {

            return false;
        }
        return true;
    }

    public function add($tab,$debug = 0)
    {
        $args['nom_alb'] = $tab['nom_alb'];
        $args['desc_alb'] = $tab['desc_alb'];
        $args['acces_alb'] = $tab['acces_alb'];
        $args['id_per'] = $_SESSION['id'];

        $query = "INSERT INTO t_albums SET "
            . "nom_alb = :nom_alb, "
            . "desc_alb = :desc_alb, "
            . "acces_alb = :acces_alb, "
            . "id_per = :id_per";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return $this->pdo->lastInsertId();
        } catch (Exeption $e) {
            return false;
        }
    }

    public function check_nom($nomAlb, $idPer){
        $query = "SELECT * FROM t_albums WHERE (nom_alb = :nom_alb AND id_per = :id_per) LIMIT 1";

        try {

            $stmt = $this->pdo->prepare($query);
            $args['nom_alb'] = $nomAlb;
            $args['id_per'] = $idPer;
            $stmt->execute($args);

            if(empty($stmt->fetch())){
                return true;
            }
            return false;

        } catch (Exception $e) {

            return false;
        }
    }

    public function set_order_pho($tab,$debug=0){

        $query = "SELECT order_pho FROM t_alb_pho "
               . "WHERE id_pho=:id_pho";
        $args['id_pho'] = $tab['id_pho'];
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
        $tab_result['result']['pos_start_pho'] = $stmt->fetch()['order_pho'];

        if($tab_result['result']['pos_start_pho'] != $tab['pos_pho']) {
            if($tab_result['result']['pos_start_pho'] > $tab['pos_pho']) {
                $query = "UPDATE t_alb_pho SET order_pho = (order_pho + 1) "
                    . "WHERE id_alb=:id_alb AND order_pho>=:order_end_pho AND order_pho<:order_start_pho";
            }else{
                $query = "UPDATE t_alb_pho SET order_pho = (order_pho - 1) "
                    . "WHERE id_alb=:id_alb AND order_pho<=:order_end_pho AND order_pho>:order_start_pho";
            }
            $args = array();
            $args['id_alb'] = $this->get_id();
            $args['order_start_pho'] = $tab_result['result']['pos_start_pho'];
            $args['order_end_pho'] = $tab['pos_pho'];
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);

            $query = "UPDATE t_alb_pho SET order_pho =:order_end_pho "
                . "WHERE id_pho=:id_pho AND id_alb =:id_alb";
            $args = array();
            $args['order_end_pho'] = $tab['pos_pho'];
            $args['id_alb'] = $this->get_id();
            $args['id_pho'] = $tab['id_pho'];
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);

            if ($debug) {
                echo "<pre>";
                print_r($args);
                echo "<br>";
                print_r($tab);
                echo "<br>";
                print_r($tab_result);
                echo "</pre>";
            }
            return true;
        }
        return false;
    }

    /**
     * Récupération de tous les photos de l'album
     * @return array
     */
    public function get_pho_alb($debug = 0)
    {
        $query = "SELECT * FROM t_albums ALB "
                ."JOIN t_alb_pho ALP ON ALB.id_alb = ALP.id_alb "
                ."JOIN t_photos PHO ON ALP.id_pho = PHO.id_pho "
                ."WHERE ALP.id_alb = :id_alb ORDER BY ALP.order_pho";

        $args['id_alb'] = $this->get_id();
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
        $tab = $stmt->fetchAll();
        return $tab;
    }
    /**
     * Récupération de tous les albums
     * @parm $order
     * @return array | bool
     */
    public function get_albums($order = "nom_alb") {

        $args[':order'] = $order;
        $query = "SELECT * FROM t_albums ORDER BY :order";

        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab_alb = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return($tab_alb);

        } catch(Exception $e) {

            return false;
        }
    }

    public function get_filter_alb($filter){
        $query = "SELECT id_alb AS id, nom_alb AS value"
                ."FROM t_albums "
                ."WHERE nom_alb LIKE :term"
                ."ORDER by nom_alb";
        $args = array(":term" => $filter."%");
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
        $tab = $stmt->fetchAll();
    }

    public function get_details($id=0, $debug=0){
        if(!$id){
            $id = $this->get_id();
        }
        $args['id_alb'] = $id;
        $query = "SELECT * FROM t_albums "
            ."WHERE id_alb=:id_alb";

        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
        $tab = $stmt->fetch();

        $query = "SELECT * FROM t_albums ALB "
            ."JOIN t_alb_tag TAA ON ALB.id_alb = TAA.id_alb "
            ."JOIN t_tags tag ON TAA.id_tag = tag.id_tag "
            ."WHERE ALB.id_alb=:id_alb";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
        $tab['tag'] = $stmt->fetchAll();

        $tab['nom_alb'] = $this->get_nom();
        $tab['desc_alb'] = $this->get_desc();
        $tab['acces_alb'] = $this->get_acces();

        return $tab;
    }
    public function add_tag($tab){
        $query = "SELECT id_tag from t_tags "
            ."WHERE nom_tag =:nom_tag";
        $args['nom_tag'] = $tab['nom_tag'];
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
        $tab += $stmt->fetch();

        if($tab['id_tag']) {
            $query = "INSERT INTO t_alb_tag SET id_alb =:id_alb, id_tag =:id_tag";
            $args = array();
            $args['id_alb'] = $this->get_id();
            $args['id_tag'] = $tab['id_tag'];
            $stmt = $this->pdo->prepare($query);
            $tab['execute'] = $stmt->execute($args);
            return $tab;
        }
        return false;
    }

    /**
     * Récupération de tous les tags non associé à l'album sélectionné
     * @author cp-17nod
     * @return array
     */
    public function get_all_tag_not_used(){
        $query = "SELECT DISTINCT(TAG.id_tag), TAG.nom_tag FROM t_tags TAG "
            ."LEFT JOIN t_alb_tag TAA on TAG.id_tag = TAA.id_tag "
            ."WHERE TAG.id_tag NOT IN(SELECT id_tag FROM t_alb_tag WHERE id_alb = :id_alb)";

        try {
            $args['id_alb'] = $this->get_id();
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return($tab);

        } catch(Exception $e) {

            return false;
        }
    }

    public function remove_tag($id_alb_tag,$debug=0){
        $query = "DELETE FROM t_alb_tag WHERE id_alb_tag=:id_alb_tag";
        $args['id_alb_tag'] = $id_alb_tag;
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
    }

    public function set_id($id) {
        $this->id = $id;
    }
    public function get_id() {
        return $this->id;
    }

    public function set_nom($nom) {
        $this->nom = $nom;
    }
    public function get_nom() {
        return $this->nom;
    }

    public function set_desc($desc) {
        $this->desc = $desc;
    }
    public function get_desc() {
        return $this->desc;
    }

    public function set_acces($acces) {
        $this->acces = $acces;
    }
    public function get_acces() {
        return $this->acces;
    }
}