<?php

Class Tag EXTENDS Projet
{
    private $id;
    private $nom;

    public function __construct($id = null) {

        $this->table_name = "t_tags";
        $this->suffix = "_tag";

        parent::__construct();

        if($id){
            $this->set_id($id);
            $this->init();
        }
    }

    public function init() {

        $query = "SELECT * FROM t_tags WHERE id_tag=:id_tag";
        try {

            $stmt = $this->pdo->prepare($query);
            $args['id_tag'] = $this->get_id();
            $stmt->execute($args);
            $tab = $stmt->fetch();

            $this->set_nom($tab['nom_tag']);
            return true;
        } catch (Exception $e) {

            return false;
        }
        return true;
    }

    public function add($tab,$debug=0)
    {
        $args['nom_tag'] = $tab['nom_tag'];

        $query = "INSERT INTO t_tags SET "
            . "nom_tag = :nom_tag";

        try {
            $stmt = $this->pdo->prepare($query);
             $stmt->execute($args);
            return $this->pdo->lastInsertId();
        } catch (Exeption $e) {
            return false;
        }
    }

    public function get_tags($order = "nom_tag") {

        $args[':order'] = $order;
        $query = "SELECT * FROM t_tags ORDER BY :order";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab_tag = $stmt->fetchAll();
            return $tab_tag;

        } catch(Exception $e) {

            return false;
        }
    }

    /**
     * Récupération de toutes les photos correspondantes au tags recherché
     * @author cp-17nod
     * @return array
     */
    public function get_pho_tag($tab,$debug = 0)
    {
        $query = "SELECT * FROM t_photos PHO "
            ."JOIN t_pho_tag TAP ON PHO.id_pho = TAP.id_pho "
            ."JOIN t_tags TAG ON TAP.id_tag = TAG.id_TAG "
            ."WHERE TAG.nom_tag = :nom_tag";

        $stmt = $this->pdo->prepare($query);
        $stmt->execute($tab);
        $tab = $stmt->fetchAll();
        return $tab;
    }

    public function set_id($id) {
        $this->id = $id;
    }
    public function get_id() {
        return $this->id;
    }

    public function set_nom($nom) {
        $this->nom = $nom;
    }
    public function get_nom() {
        return $this->nom;
    }
}