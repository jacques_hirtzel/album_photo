<?php
require_once substr(__dir__, 0, strpos(__dir__, "album_photo")+strlen("album_photo")) . "/config/config.inc.php";
$aut = "ADM_AUT";
require(WAY . "includes/secure.inc.php");
require_once(WAY . "includes/head.inc.php");
$tag = new Tag();
$tab_tag = $tag->get_tags();

?>
<div class="col-md-12">
    <div class="panel panel-primary">

        <div class="panel-heading">
            <h3>
                <?php
                echo "Modifcation des Tags";
                echo"<button id=\"plus\" type='submit' class='btn btn-success pull-right' onclick=\"window.location='./newTag.php'\">";
                echo"<span class='glyphicon glyphicon-plus' aria-hidden='true'> </span>";
                echo"</button> ";
                ?>
            <h3>
        </div>

        <div class="panel-body">
            <table class="table table-condensed table-bordered">
                <thead>
                    <th class="background gray">Nom du Tag</th>
                </thead>
                <tbody>
                <?php

                foreach($tab_tag as $tags) {
                    echo "<tr>";
                    echo "<td>";

                    echo "<span>" . $tags['nom_tag'] . "</span>";
                    echo "<button type=\"button\" class=\"btn btn-danger pull-right btn_trash btn-xs glyphicon glyphicon-trash\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Supression\" id_tag=\"".$tags['id_tag']."\"></button>";

                    echo "</td>";
                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>

        <div class="panel-footer">

        </div>

    </div>
</div>

<script src="js/del_tag.js"></script>