<?php
require_once substr(__dir__, 0, strpos(__dir__, "album_photo")+strlen("album_photo")) . "/config/config.inc.php";
$aut = "ADM_USR";
require(WAY . "includes/secure.inc.php");
require_once(WAY . "includes/autoload.inc.php");
require_once(WAY . "includes/head.inc.php");
?>

<div class="col-md-12">
    <div class="panel panel-primary">

        <div class="panel-heading">
            Création d'un nouveau tag
        </div>

        <div class="panel-body">
            <form id="newTag_form">

                <div class="form-group row">
                    <label for="nom_alb" class="col-sm-2 col-form-label">Nom</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nom_tag" name="nom_tag" placeholder="Nom du tag">
                    </div>
                </div>

                <div class="form-group action-button">
                    <input type="submit" class="btn btn-success" value="Créer">
                    <a href="./index.php"><button type="button" class="btn btn-warning">Annuler</button></a>
                </div>
            </form>
        </div>

        <div class="panel-footer">

        </div>

    </div>

</div>
<script src="js/newTag.js"></script>
