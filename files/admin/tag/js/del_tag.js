
$(".btn_trash").on("click",function (){
    id_tag = $(this).attr("id_tag");
    bootbox.confirm({
        title: "Supprimer un tag",
        message: "Êtes-vous sûr de vouloir <b>supprimer ce tag</b> ? ",
        buttons: {
            cancel: {
                label: 'Annuler'
            },
            confirm: {
                label:  'Supprimer'
            }
        },
        callback: function (result) {
            if(result == true) {
                del_tag(id_tag);
            }
        }
    });
});

function del_tag(id_tag){
    $.post(
        "./json/del_tag.json.php",
        {
            id_tag: id_tag
        },
        function(data){
            location.reload();
        }
    )
}