$(function(){
    $("#newTag_form").validate(
        {
            rules:{
                nom_tag: {
                    required: true,
                    minlength: 2
                }
            },
            messages: {
                nom_tag: {
                    required: "Veuillez saisir un nom de tag",
                    minlength: "Votre le nom de tag doit être composé de 2 caractères au minimum"
                }
            },
            submitHandler: function(form) {
                $.post(
                    "./json/newTag.json.php?_="+Date.now(),
                    {
                        nom_tag: $("#nom_tag").val()
                    },

                    function result(data, status){
                        message(data.message.texte, data.message.type);
                    }
                );
            }
        }
    );
});
