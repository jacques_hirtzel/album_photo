<?php
header('Content-Type: application/json');
require_once substr(__dir__, 0, strpos(__dir__, "album_photo")+strlen("album_photo")) . "/config/config.inc.php";
$aut = "ADM_AUT";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$tag = new Tag();

if($tag->remove($_POST['id_tag'])){
    $tab['reponse'] = true;
    $tab['message']['texte'] = "Le tag a bien été effacée.";
    $tab['message']['type'] = "success";
}else {
    $tab['response'] = false;
    $tab['message']['texte'] = "Le tag n'a pas pu être effacée !";
    $tab['message']['type'] = "danger";
}

echo json_encode($tab);