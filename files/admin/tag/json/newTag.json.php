<?php
header('Content-Type: application/json');
session_start();
require_once substr(__dir__, 0, strpos(__dir__, "album_photo")+strlen("album_photo")) . "/config/config.inc.php";
require_once(WAY . "/includes/autoload.inc.php");

$tag = new Tag();
$id = $tag->add($_POST);
if($id){
    $tag->set_id($id);
    if($tag->init()) {
        $tab['response'] = true;
        $tab['message']['texte'] = "Le nouveau tag à bien été créé<br><a href=\"../tag/index.php\">Vos tags</a>";
        $tab['message']['type'] = "success";
    }
}
else{
    $tab['response'] = false;
    $tab['message']['texte'] = "Création impossible, vous possédez déjà un tag appelé comme ça";
    $tab['message']['type'] = "danger";
}

echo json_encode($tab);
?>