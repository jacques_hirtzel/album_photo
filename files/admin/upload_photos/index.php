<?php
require_once substr(__dir__, 0, strpos(__dir__, "album_photo")+strlen("album_photo")) . "/config/config.inc.php";
$aut = "USR_INT";
require(WAY . "includes/secure.inc.php");
require_once(WAY . "includes/autoload.inc.php");
require_once(WAY . "includes/head.inc.php");
?>
<script src="./js/upload.js"></script>

<div class="col-md-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            Ajouter des photos
        </div>
        <div class="panel-body">
            <form action="upload.php" class="dropzone"></form>
        </div>
    </div>
</div>