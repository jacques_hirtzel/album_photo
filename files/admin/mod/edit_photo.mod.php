<!-- Modal -->
<div class="modal fade" id="edit_photo" role="dialog" aria-labelledby="edit_photo" aria-hidden="true">
    <input type="hidden" id="id_pho" name="id_pho">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Modification de la photo</h4>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-12 text-center">
                        <img id="image-mod">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="titre_pho" class="col-sm-2 col-form-label">Titre</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="titre_pho" name="titre_pho" placeholder="Titre de la photo">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="desc_pho" class="col-sm-2 col-form-label">Description</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="desc_pho" name="desc_pho" placeholder="Description de la photo">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Albums</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <div class="form-control alb-container">
                            </div>
                            <span class="input-group-btn"><button id="open_alb_mod" class="btn btn-primary" type="button" data-toggle="modal" data-target="#add_alb_pho"><span class="glyphicon glyphicon-plus"></span></button></span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Tags</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <div class="form-control tag-container">
                            </div>
                            <span class="input-group-btn"><button id="open_tag_mod" class="btn btn-primary" type="button" data-toggle="modal" data-target="#add_pho_tag"><span class="glyphicon glyphicon-plus"></span></button></span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="filename_pho" class="col-sm-2 col-form-label">Filename</label>
                    <div class="col-sm-10">
                        <input disabled="true" type="text" class="form-control" id="filename_pho" name="filename_pho" placeholder="Nom du fichier">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="model_pho" class="col-sm-2 col-form-label">Model</label>
                    <div class="col-sm-10">
                        <input disabled="true" type="text" class="form-control" id="model_pho" name="model_pho" placeholder="Inconnu">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="software_pho" class="col-sm-2 col-form-label">Software</label>
                    <div class="col-sm-10">
                        <input disabled="true" type="text" class="form-control" id="software_pho" name="software_pho" placeholder="Inconnu">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="Delete">Delete</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="Save">Save changes</button>
            </div>
        </div>
    </div>
</div>
<br>

<?php
    include(WAY . "admin/albums/mod/add_alb_pho.mod.php");
    include(WAY . "admin/tag/mod/add_pho_tag.mod.php");
?>
