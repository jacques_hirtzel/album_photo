<?php
header('Content-Type: application/json');
require_once substr(__dir__, 0, strpos(__dir__, "album_photo")+strlen("album_photo")) . "/config/config.inc.php";
$aut = "USR_INT";
require(WAY . "includes/secure.inc.php");
require_once(WAY . "includes/autoload.inc.php");

$alb = new Album();
$tab = $alb->remove_tag($_POST['id_alb_tag']);

echo json_encode($tab);
?>