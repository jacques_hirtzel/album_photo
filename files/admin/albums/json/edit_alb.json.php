<?php
header('Content-Type: application/json');
require_once substr(__dir__, 0, strpos(__dir__, "album_photo")+strlen("album_photo")) . "/config/config.inc.php";
$aut = "USR_UPL";
require(WAY . "includes/secure.inc.php");
require_once(WAY . "includes/autoload.inc.php");

$alb = new Album($_POST['id_alb']);
$tab = $_POST;

if($alb->update($_POST)){
    $tab['reponse'] = true;
    $tab['message']['texte'] = "Changement engregistré.";
    $tab['message']['type'] = "success";
}else {
    $tab['response'] = false;
    $tab['message']['texte'] = "Changement invalide !";
    $tab['message']['type'] = "danger";
}
echo json_encode($tab);