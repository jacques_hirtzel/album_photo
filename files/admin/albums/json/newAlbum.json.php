<?php
header('Content-Type: application/json');
session_start();
require_once substr(__dir__, 0, strpos(__dir__, "album_photo")+strlen("album_photo")) . "/config/config.inc.php";
require_once(WAY . "/includes/autoload.inc.php");

$alb = new Album();

if($alb->check_nom($_POST['nom_alb'],$_SESSION["id"])){
    $id = $alb->add($_POST);
    $alb->set_id($id);
    if($alb->init()) {

        $tab['response'] = true;
        $tab['message']['texte'] = "Le nouvel album à bien été créé<br><a href=\"../albums/index.php\">Vos albums</a>";
        $tab['message']['type'] = "success";
    }
}
else{
    $tab['response'] = false;
    $tab['message']['texte'] = "Création impossible, vous possédez déjà un album appelé comme ça";
    $tab['message']['type'] = "danger";
}

echo json_encode($tab);
?>