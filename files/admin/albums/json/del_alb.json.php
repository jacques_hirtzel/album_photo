<?php
header('Content-Type: application/json');
session_start();
require_once substr(__dir__, 0, strpos(__dir__, "album_photo")+strlen("album_photo")) . "/config/config.inc.php";
require_once(WAY . "/includes/autoload.inc.php");

$alb = new Album();
if($alb->remove($_POST['id_alb'])){
    $tab['reponse'] = true;
    $tab['message']['texte'] = "L'album a été effacée.";
    $tab['message']['type'] = "success";
}else {
    $tab['response'] = false;
    $tab['message']['texte'] = "L'album n'a pas pu être effacée !";
    $tab['message']['type'] = "danger";
}

echo json_encode($tab);
?>