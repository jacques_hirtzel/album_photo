$(function(){
    $("#newAlbum_form").validate(
        {
            rules:{
                nom_alb: {
                    required: true,
                    minlength: 2
                },
                desc_alb: {
                    required: true,
                    minlength: 2
                },
                acces_alb: {
                    required: true
                }
            },
            messages: {
                nom_alb: {
                    required: "Veuillez saisir un nom d'album",
                    minlength: "Votre le nom de l'album doit être composé de 2 caractères au minimum"
                },
                desc_alb: {
                    required: "Veuillez saisir une description de votre album",
                    minlength: "Votre la description doit être composé de 2 caractères au minimum"
                },
                access_alb: {
                    required: "Veuillez choisir un type d'accès pour votre album",
                }
            },
            submitHandler: function(form) {
                $.post(
                    "./json/newAlbum.json.php?_="+Date.now(),
                    {
                        nom_alb: $("#nom_alb").val(),
                        desc_alb: $("#desc_alb").val(),
                        acces_alb: $("#acces_alb").val()
                    },

                    function result(data, status){
                        message(data.message.texte, data.message.type);
                    }
                );
            }
        }
    );
});
