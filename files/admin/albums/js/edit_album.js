$(function sortable() {
    $(".sortable").sortable({
        start: function (event, ui) {
            id_pho = ui.item.attr("id");
        },

        stop: function (event, ui) {
            $.post(
                "./json/sortable.json.php?_=" + Date.now(),
                {
                    pos_pho: ui.item.index(),
                    id_alb: $("#id_alb").val(),
                    id_pho: id_pho
                },
                function (data, status) {
                    /*$("#loading_zone").load(
                        "./load/album.load.php",
                        {
                            id_alb : data.id_alb
                        }
                    );*/
                }
            );
        }
    });

    $(".sortable").disableSelection()
    $(".sortable").on("sortstart", function (event, ul) {
    });
    $(".sortable").on("sortstop", function (event, ul) {
    });

    $("#moins").click(function () {

        bootbox.confirm({
            title: "Supprimer un album",
            message: "Êtes-vous sûr de vouloir <b>supprimer cet album</b>",
            buttons: {
                cancel: {
                    label: 'Annuler'
                },
                confirm: {
                    label: 'Supprimer'
                }
            },
            callback: function (result) {
                if (result == true) {
                    del_alb($("#id_alb").val());
                }
            }
        });
    });

    function del_alb(id_alb){
        $.post(
            "./json/del_alb.json.php",
            {
                id_alb: id_alb
            },
            function result(data, status){
                message(data.message.texte, data.message.type);

                if(data.message.type == "success"){
                    location.replace("./index.php");
                }
            }
        );
    }

    $("#edit").click(function () {
        $("#edit_album").modal('show');
        $.getJSON(
            "./json/get_alb_details.json.php",
            {
                id_alb: $("#id_alb").val()
            },
            function(data) {
                $.each(data,function(key,val){
                    $("#"+key).val(val);
                });
            }
        );
    });

    $(".tag-container").on("click", ".remove_tag", function(){
        var id_alb_tag = $(this).attr('data-id_alb_tag');
        $.post(
            "./json/del_alb_tag.json.php",
            {
                id_alb_tag: id_alb_tag
            },
            function result(data, status) {
                $(`.tag[data-id_alb_tag=${id_alb_tag}]`).remove();
            }
        );
    });

    $("#open_tag_mod").click(function () {
        $.post(
            "./json/load-list_tag.json.php",
            {
                id_alb: $("#id_alb").val()
            },
            function result(data,status) {
                countries = data;
                autocomplete(document.getElementById("new_tag"), countries);
            }
        )
    });
    
    $("#Save_alb_tag").click(function () {
        $.post(
            "./json/add_alb_tag.json.php",
            {
                id_alb: $("#id_alb").val(),
                nom_tag: $("#new_tag").val()
            },
            function result(data, status){
                if(data.execute) {
                    $(".tag-container").append("<span class=\"label label-primary tag\" data-id_alb_tag=\"" + data.id_tag + "\">" + data.nom_tag + "<span class=\"remove_tag\" data-id_alb_tag=\"" + data.id_tag + "\"> &times;</span> </span>");
                }
            }
        );
    });

    $("#Save").click(function () {
        $.post(
            "./json/edit_alb.json.php",
            {
                id_alb: $("#id_alb").val(),
                nom_alb: $(".modal-body #nom_alb").val(),
                desc_alb: $(".modal-body #desc_alb").val(),
                acces_alb: $(".modal-body #acces_alb").val()
            },
            function result(data, status){
                message(data.message.texte, data.message.type);
                $("#nom").html(data.nom_alb);
                $("#desc").html(data.desc_alb);
                $("#acces").html(data.acces_alb);
            }
        );
    })


    function autocomplete(inp, arr) {
        /*the autocomplete function takes two arguments,
        the text field element and an array of possible autocompleted values:*/
        var currentFocus;
        /*execute a function when someone writes in the text field:*/
        inp.addEventListener("input", function(e) {
            var a, b, i, val = this.value;
            /*close any already open lists of autocompleted values*/
            closeAllLists();
            if (!val) { return false;}
            currentFocus = -1;
            /*create a DIV element that will contain the items (values):*/
            a = document.createElement("DIV");
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            /*append the DIV element as a child of the autocomplete container:*/
            this.parentNode.appendChild(a);
            /*for each item in the array...*/
            for (i = 0; i < arr.length; i++) {
                /*check if the item starts with the same letters as the text field value:*/
                if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                    /*create a DIV element for each matching element:*/
                    b = document.createElement("DIV");
                    /*make the matching letters bold:*/
                    b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                    b.innerHTML += arr[i].substr(val.length);
                    /*insert a input field that will hold the current array item's value:*/
                    b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                    /*execute a function when someone clicks on the item value (DIV element):*/
                    b.addEventListener("click", function(e) {
                        /*insert the value for the autocomplete text field:*/
                        inp.value = this.getElementsByTagName("input")[0].value;
                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();
                    });
                    a.appendChild(b);
                }
            }
        });
        /*execute a function presses a key on the keyboard:*/
        inp.addEventListener("keydown", function(e) {
            var x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");
            if (e.keyCode == 40) {
                /*If the arrow DOWN key is pressed,
                increase the currentFocus variable:*/
                currentFocus++;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 38) { //up
                /*If the arrow UP key is pressed,
                decrease the currentFocus variable:*/
                currentFocus--;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 13) {
                /*If the ENTER key is pressed, prevent the form from being submitted,*/
                e.preventDefault();
                if (currentFocus > -1) {
                    /*and simulate a click on the "active" item:*/
                    if (x) x[currentFocus].click();
                }
            }
        });
        function addActive(x) {
            /*a function to classify an item as "active":*/
            if (!x) return false;
            /*start by removing the "active" class on all items:*/
            removeActive(x);
            if (currentFocus >= x.length) currentFocus = 0;
            if (currentFocus < 0) currentFocus = (x.length - 1);
            /*add class "autocomplete-active":*/
            x[currentFocus].classList.add("autocomplete-active");
        }
        function removeActive(x) {
            /*a function to remove the "active" class from all autocomplete items:*/
            for (var i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
            }
        }
        function closeAllLists(elmnt) {
            /*close all autocomplete lists in the document,
            except the one passed as an argument:*/
            var x = document.getElementsByClassName("autocomplete-items");
            for (var i = 0; i < x.length; i++) {
                if (elmnt != x[i] && elmnt != inp) {
                    x[i].parentNode.removeChild(x[i]);
                }
            }
        }
        /*execute a function when someone clicks in the document:*/
        document.addEventListener("click", function (e) {
            closeAllLists(e.target);
        });
    }
});
