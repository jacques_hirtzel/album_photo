<!-- Modal -->
<div class="modal fade" id="edit_album" tabindex="-1" role="dialog" aria-labelledby="edit_album" aria-hidden="true">
    <input type="hidden" id="id_alb" name="id_alb">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Modification de l'album</h4>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="nom_alb" class="col-sm-2 col-form-label">Nom</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nom_alb" name="nom_alb" placeholder="Nom de l'album">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="desc_alb" class="col-sm-2 col-form-label">Description</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="desc_alb" name="desc_alb" placeholder="Description de l'album">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Tags</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <div class="form-control tag-container">
                            </div>
                            <span class="input-group-btn"><button id="open_tag_mod" class="btn btn-primary" type="button" data-toggle="modal" data-target="#add_alb_tag"><span class="glyphicon glyphicon-plus"></span></button></span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="acces_alb" class="col-sm-2 col-form-label">Accessibilité de l'album</label>
                    <div class="col-sm-10">
                        <select class="form-control" id="acces_alb" name="acces_alb">
                            <option value="1">Privé</option>
                            <option value="2">Non-répertorié</option>
                            <option value="3">Public</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="Save">Save changes</button>
            </div>
        </div>
    </div>
</div>

<?php
include(WAY . "admin/albums/mod/add_alb_tag.mod.php");
?>