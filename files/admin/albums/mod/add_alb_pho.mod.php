<!-- Modal -->
<div class="modal fade" id="add_alb_pho" tabindex="-1" role="dialog" aria-labelledby="add_alb_pho" aria-hidden="true">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Ajout d'un ablum</h4>
            </div>
            <div class="modal-body">
                Entrez ici le nom du nouvel album dont-elle va faire partie<br><br>
                <div class="form-group row">
                    <label for="recherche_alb_pho" class="col-sm-2 col-form-label">Album</label>
                    <div class="col-sm-10">
                        <input id="new_alb" class="form-control" type="text" placeholder="Album">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="Save_alb_pho">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>