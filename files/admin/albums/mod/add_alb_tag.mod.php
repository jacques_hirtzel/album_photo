<!-- Modal -->
<div class="modal fade" id="add_alb_tag" tabindex="-1" role="dialog" aria-labelledby="add_alb_tag" aria-hidden="true">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Ajout d'un tag</h4>
            </div>
            <div class="modal-body">
                Entrez ici le nom du nouveau Tag dont-il va être assigné<br><br>
                <div class="form-group row">
                    <label for="recherche_alb_tag" class="col-sm-2 col-form-label">Tag</label>
                    <div class="col-sm-10">
                        <input id="new_tag" class="form-control" type="text" placeholder="Tag">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="Save_alb_tag">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>