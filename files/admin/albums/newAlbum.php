<?php
require_once substr(__dir__, 0, strpos(__dir__, "album_photo")+strlen("album_photo")) . "/config/config.inc.php";
$aut = "USR_INT";
require(WAY . "includes/secure.inc.php");
require_once(WAY . "includes/autoload.inc.php");
require_once(WAY . "includes/head.inc.php");
?>

<div class="col-md-12">
    <div class="panel panel-primary">

        <div class="panel-heading">
            Création d'un nouvel album
        </div>

        <div class="panel-body">
            <form id="newAlbum_form">

                <div class="form-group row">
                    <label for="nom_alb" class="col-sm-2 col-form-label">Nom</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nom_alb" name="nom_alb" placeholder="Nom de l'album">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="desc_alb" class="col-sm-2 col-form-label">Description</label>
                    <div class="col-sm-10">
                        <textarea type="text" class="form-control" id="desc_alb" name="desc_alb" placeholder="Description de l'album"></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="acces_alb" class="col-sm-2 col-form-label">Accessibilité de l'album</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="acces_alb" id="acces_alb">
                            <option value="1" selected >Privé</option>
                            <option value="2">Non-répertorié</option>
                            <option value="3">Public</option>
                        </select>
                    </div>
                </div>

                <div class="form-group action-button">
                    <input type="submit" class="btn btn-success" value="Créer">
                    <a href="./index.php"><button type="button" class="btn btn-warning">Annuler</button></a>
                </div>

            </form>
        </div>

        <div class="panel-footer">

        </div>

    </div>

</div>
<script src="js/newAlbum.js"></script>
