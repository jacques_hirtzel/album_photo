<?php
require_once substr(__dir__, 0, strpos(__dir__, "album_photo")+strlen("album_photo")) . "/config/config.inc.php";
$aut = "USR_INT";
require(WAY . "includes/secure.inc.php");
require_once(WAY . "includes/autoload.inc.php");

$id_alb = $_POST['id_alb'];
$alb = new Album($id_alb);

$tab_pho = $alb->get_pho_alb();
?>
<link rel="stylesheet" href="./css/alb.css">
<input type="hidden" id="id_alb" value="<?= $id_alb ?>">
<div class="col-md-12">
    <div class="panel panel-primary">

        <div class="panel-heading">
            <h3>
                <?php
                echo "<button id=\"retour\" onclick=\"window.location='./index.php'\" class=\"btn btn-primary glyphicon glyphicon-chevron-left\"></button> ";
                echo "<span id=\"nom\"> ".$alb->get_nom()."</span>";
                echo"<button id=\"moins\" type='submit' class='btn btn-danger pull-right glyphicon glyphicon-trash'/>";
                echo"<button id=\"edit\" type='submit' class='btn btn-warning pull-right glyphicon glyphicon-pencil'/>";
                echo "<br>";
                echo "<span id=\"desc\"> ".$alb->get_desc()."</span>";
                ?>
            </h3>
        </div>

        <div class="panel-body">
        <?php

        echo "<ul class='sortable'>";
        foreach ($tab_pho as $photo) {
            $pho = new Photo($photo['id_pho']);
            echo "<li id=\"".$photo['id_pho']."\"><img src=\"". URL."view/img_view.php?file=".$pho->get_way()."\"></li>";
        }
        echo "</ul>";

        ?>
        </div>
        <div class="panel-footer">
        </div>
    </div>
</div>
<script src="<?=URL?>admin/albums/js/edit_album.js"></script>

<?php
include(WAY . "admin/albums/mod/edit_album.mod.php");
?>
