<?php
require_once substr(__dir__, 0, strpos(__dir__, "album_photo")+strlen("album_photo")) . "/config/config.inc.php";
$aut = "USR_INT";
require(WAY . "includes/secure.inc.php");
require_once(WAY . "includes/autoload.inc.php");
?>
<link rel="stylesheet" href="./css/alb.css">

<div class="panel panel-primary" >

    <div class="panel-heading">
        <h3>
            <?php
            if(!isset($_GET['nom_alb'])) {
                echo "Vos albums";
                echo"<button id=\"plus\" type='submit' class='btn btn-success pull-right' onclick=\"window.location='./newAlbum.php'\">";
                echo"<span class='glyphicon glyphicon-plus' aria-hidden='true'> </span>";
                echo"</button> ";
            }else{
                echo"<button type='submit' class='btn btn-primary' onclick=\"window.location='./index.php'\">";
                echo"<span class='glyphicon glyphicon-chevron-left' aria-hidden='true'> </span>";
                echo"</button> ";
                echo"<span>".$_GET['nom_alb']."</span>";
            }
            ?>
        </h3>
    </div>

    <div class="panel-body">
        <?php
        if(!isset($_GET['nom_alb'])) {
            $tab_alb = $per->get_all_alb();

            $nb_alb = count($tab_alb);
            $str = "";
            if (!$nb_alb) {
                $str .= "Vous ne possédez pas d'album";
            } else {
                $str .= "Vous possédez " . count($tab_alb) . " album" . ($nb_alb > 1 ? "s : " : " : ");
            }
            echo "<p>" . $str . "</p>";
            echo "<br>";

            /*echo "<pre>";
            print_r($_GET);
            echo "</pre>";*/

            foreach ($tab_alb as $album) {
                $alb = new Album($album['id_alb']);
                $tab_pho = $alb->get_pho_alb();

                //echo "<a href='index.php?nom_alb=" . $album['nom_alb'] . "'>" . $album['nom_alb'] . "</a>";
                echo "<span class=\"link_album\" id_alb=\"" . $album['id_alb'] . "\">" . $album['nom_alb'] . "</span>";

                $nb_pho = count($tab_pho);
                $str = "";
                if (!$nb_pho) {
                    $str .= " (Aucune photo) ";
                } else {
                    $str .= " (" . count($tab_pho) . " photo" . ($nb_pho > 1 ? "s) " : ") ");
                }
                echo $str;
                echo "<br>";
                echo "<br>";
            }
        }
        ?>
    </div>


    <div class="panel-footer">

    </div>
</div>