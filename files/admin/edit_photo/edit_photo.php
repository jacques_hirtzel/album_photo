<?php

require_once substr(__dir__, 0, strpos(__dir__, "album_photo")+strlen("album_photo")) . "/config/config.inc.php";
$aut = "USR_INT";
require(WAY . "includes/secure.inc.php");
require_once(WAY . "includes/autoload.inc.php");
require_once(WAY . "includes/head.inc.php");

$pho = new Photo();
$tag = new Tag();
$per = new Personne($_SESSION['id']);

$tab_photo = $per->get_pho_per();
?>

<div class="col-md-12">
    <div class="panel panel-primary">

        <div class="panel-heading">
            <h3 style="float: left">Photos</h3>

            <div class="form-group row">
                <div class="col-sm-4" style=" float: right; margin-top: 15px">
                    <div class="input-group">
                        <input id="search_pho_tag" type="text" class="form-control" placeholder="Rechercher avec un tag">
                        <div class="input-group-btn">
                            <button id="search_pho" class="btn btn-info" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel-body" id="loading_zone">
            <?php
            $nb_pho = count($tab_photo);
            $str = "Vous ";
            if (!$nb_pho) {
                $str .= "ne possédez aucune photo ";
            } else {
                $str .= "possédez " . count($tab_photo) . " photo" . ($nb_pho > 1 ? "s " : " ");
            }
            echo $str;

            ?>

            <?php
            echo "<br>";
            foreach ($tab_photo as $photo){
                $pho = new Photo($photo['id_pho']);
                ?>
                <a href="#" id_pho="<?= $photo['id_pho'] ?>" class="btn_edit_photo">
                    <img src="<?= URL."view/img_view.php?file=".$pho->get_way(); ?>">
                </a>
            <?php } ?>
        </div>
        <div class="panel-footer">
        </div>
    </div>
</div>

<?php

include(WAY . "/admin/mod/edit_photo.mod.php");

?>

<script src="./js/edit_photo.js"></script>
