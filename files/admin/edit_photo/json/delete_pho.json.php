<?php
header('Content-Type: application/json');
require_once substr(__dir__, 0, strpos(__dir__, "album_photo")+strlen("album_photo")) . "/config/config.inc.php";
$aut = "USR_INT";
require(WAY . "includes/secure.inc.php");
require_once(WAY . "includes/autoload.inc.php");

$pho = new Photo($_POST['id_pho']);
$tab = array();
$tab['id_pho'] = $_POST['id_pho'];

if($pho->remove($_POST['id_pho'])){
    $tab['reponse'] = true;
    $tab['message']['texte'] = "La photo a été effacée.";
    $tab['message']['type'] = "success";
}else {
    $tab['response'] = true;
    $tab['message']['texte'] = "La photo n'a pas pu être effacée !";
    $tab['message']['type'] = "danger";
}

echo json_encode($tab);