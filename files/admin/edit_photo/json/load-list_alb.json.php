<?php
header('Content-Type: application/json');
require_once substr(__dir__, 0, strpos(__dir__, "album_photo")+strlen("album_photo")) . "/config/config.inc.php";
$aut = "USR_INT";
require(WAY . "includes/secure.inc.php");
require_once(WAY . "includes/autoload.inc.php");

$pho = new Photo($_POST['id_pho']);
$tab = array();
foreach($pho->get_all_alb_not_used($_SESSION['id']) as $value){
    $tab[] = $value['nom_alb'];
}

echo json_encode($tab);
?>