$(function(){
    
    $("#fonctions_form").validate(
        {
            rules:{
                nom_fnc: {
                    required: true,
                    minlength: 3
                },
                abr_fnc: {
                    required: true,
                    minlength: 2
                },
                desc_fnc: {
                    required: true,
                    minlength: 5
                }
            }, 
            messages: {
                nom_fnc: {
                    required: "Veuillez saisir le nom de la fonction",
                    minlength: "Le nom doit être composé de 3 caractères minimum"
                },
                abr_fnc: {
                    required: "Veuillez saisir l'abreviation de la fonction",
                    minlength: "L'abreviation doit être composé de 2 caractères minimum"
                },
                desc_fnc: {
                    required: "Veuillez saisir la description de la fonction",
                    minlength: "La description doit être composé de 5 characters minimum"
                }
            },
            submitHandler: function(form) {

                $.post(
                    "../droits/json/add_fonction.json.php?_="+Date.now(),
                    {
                        nom_fnc: $("#nom_fnc").val(),
                        abr_fnc: $("#abr_fnc").val(),
                        desc_fnc: $("#desc_fnc").val()
                    },
                    function result(data, status){

                        message(data.message.texte, data.message.type);

                        if(data.response) {

                            $("#fonctions_form")[0].reset();
                        }
                    }
                );
            }
        }
    );
});
