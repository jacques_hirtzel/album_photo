

$(function(){

    $(".auth").click(function(){

        //$("#loading").css("display", "block");
        $(this).css("display", "none");
        $(this).parent().append("<img class=\"loading\" src=\"./../icones/loading.gif\" height=38px>");

        $.post(
            "./json/add_del_fnc_per.json.php?_=" + Date.now(),
            {
                id_per: $(this).attr("id_per"),
                id_fnc: $(this).attr("id_fnc"),
                status: $(this).is(":checked"),
                id_auth: $(this).attr("id")
            },
            function (data, status){
                var background = data.operation === "add" ? " #90ee97": "#ee9090";
                $("#"+data.id_auth).parent().css("background", background);

                message(data.message.texte, data.message.type);

                $("#"+data.id_auth).siblings(".loading").remove();
                $("#"+data.id_auth).css("display", "block");

                $("#loading").css("display", "none");
            }
        );

    });

    $(".btn_trash").on("click",function (){
        id_per = $(this).attr("id_per");
        bootbox.confirm({
            title: "Supprimer un utilisateur",
            message: "Êtes-vous sûr de vouloir <b>supprimer cet utilisateur</b><br>Merci d'avertir la personne concernée avant de supprimer",
            buttons: {
                cancel: {
                    label: 'Annuler'
                },
                confirm: {
                    label:  'Supprimer'
                }
            },
            callback: function (result) {
                if(result == true) {
                    del_per(id_per);
                }
            }
        });
    });

    function del_per(id_per){
        $.post(
            "./json/del_per.json.php",
            {
                id_per: id_per
            },
            function(data){
                location.reload();
            }
        )
    }

});